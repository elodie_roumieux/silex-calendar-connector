﻿using Google.GData.Calendar;
using Google.GData.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silex_GoogleCalendarConnector
{
    public class CalendarUtils
    {
        public string getFeedUriFromCalendarTitle(string username, string password, string calendarTitle)
        {
            // Authenticate
            CalendarService myService = new CalendarService("");
            myService.setUserCredentials(username, password);

            CalendarQuery query = new CalendarQuery();
            query.Uri = new Uri("https://www.google.com/calendar/feeds/default/allcalendars/full");
            CalendarFeed resultFeed = (CalendarFeed)myService.Query(query);

            foreach (CalendarEntry entry in resultFeed.Entries)
            {
                if (entry.Title.Text == calendarTitle)
                {
                    string feedUriFromCalendarTitle = "https://www.google.com/calendar/feeds/" + ParseCalendarIdFromURI(entry.EditUri) + "/private/full";
                    return feedUriFromCalendarTitle;
                }
            }
            return null;
        }

        //TODO : check "InternalsVisibleTo" to make this function private
        /// <summary>
        /// Extract the calendar id from its URI
        /// </summary>
        internal String ParseCalendarIdFromURI(AtomUri Uri)
        {
            if (Uri == null)
                return null;
            String uriString = Uri.ToString();
            int lastIndexOfSlash = uriString.LastIndexOf("/");
            if (lastIndexOfSlash == -1)
            {
                return null;
            }
            return uriString.Substring(lastIndexOfSlash + 1, uriString.Length - lastIndexOfSlash - 1);
        }
    }
}
