﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.GData.Calendar;
using Google.GData.Client;


namespace Silex_GoogleCalendarConnector
{

    public struct CalendarConnectionInformations
    {
        /// <summary>
        /// email address used to connect to the google agenda account: "xxxx@gmail.com"
        /// </summary>
        public string username;
        
        public string password;

        /// <summary>
        /// feedUri allows to connect to a specific calendar
        /// it can be the main calendar using https://www.google.com/calendar/feeds/default/private/full
        /// or a specific calendar using its id:
        /// https://www.google.com/calendar/feeds/\<id\>/private/full
        /// </summary>
        public string feedUri;
    }
}
