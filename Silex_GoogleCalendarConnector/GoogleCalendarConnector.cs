﻿using Google.GData.Calendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.GData.Client;
using Google.GData.Extensions;

namespace Silex_GoogleCalendarConnector
{
    public class GoogleCalendarConnector
    {
        private List<CalendarConnectionInformations> _connectionInformations;

        public GoogleCalendarConnector(CalendarConnectionInformations connectionInformations)
        {
            _connectionInformations = new List<CalendarConnectionInformations>{connectionInformations};
        }

        public GoogleCalendarConnector(List<CalendarConnectionInformations> connectionInformations)
        {
            _connectionInformations = connectionInformations ;
        }

        /// <summary>
        /// Returns a list of the Id of the created event in each calendar, should be stocked for future uses of the event (update/delete).
        /// </summary>
        /// <param name="connectionInformationsList">List of connectionInformation with feedUri of the different calendars.</param>
        /// <returns>Returns a list of the id of the created event in each calendar, should be stocked for future uses of the event (update/delete).</returns>
        public List<string> CreateEvent(EventEntry eventToCreate)
        {
            List<string> eventIdList = new List<string>();
            foreach (var connectionInformation in _connectionInformations)
            {
                string eventId = CreateEventInCalendar(connectionInformation, eventToCreate);

                if (eventId == null)
                {
                    return null;
                }
                eventIdList.Add(eventId);
            }
            return eventIdList;
        }




        /// <summary>
        /// Returns the id of the created event, should be stocked for future uses of the event (update/delete). 
        /// </summary>
        /// <returns>Returns the id of the created event, should be stocked for future uses of the event (update/delete). </returns>
        public string CreateEventInCalendar(CalendarConnectionInformations connectionInformations, EventEntry eventToCreate)
        {
            // Authenticate
            CalendarService service = new CalendarService("");
            service.setUserCredentials(connectionInformations.username, connectionInformations.password);
           
            Uri postUri = new Uri(connectionInformations.feedUri);

            AtomEntry insertedEntry = service.Insert(postUri, eventToCreate);
            
            //Verify the event has been created
            if (insertedEntry == null)
                return null;
            else
            {
                return insertedEntry.EditUri.ToString();
            }
        }


        /// <summary>
        /// Returns a list of the Id of the created event in each calendar, should be stocked for future uses of the event (update/delete).
        /// </summary>
        /// <param name="connectionInformationsList">List of connectionInformation with feedUri of the different calendars.</param>
        /// <returns>Returns a list of the id of the created event in each calendar, should be stocked for future uses of the event (update/delete).</returns>
        public List<string> CreateEventInMultipleCalendars(List<CalendarConnectionInformations> connectionInformationsList, EventEntry eventToCreate)
        {
            List<string> eventIdList = new List<string>();
            foreach (var connectionInformation in connectionInformationsList)
            {
                string eventId = CreateEventInCalendar(connectionInformation, eventToCreate);

                if (eventId==null)
                {
                    return null;
                }
                eventIdList.Add(eventId);
            }
            return eventIdList;
        }


        /// <summary>
        /// Create an event in a calendar if it doesn't already exist, else update it. 
        /// Returns the id of the created event, should be stocked for future uses of the event (update/delete). 
        /// </summary>
        /// <param name="eventToCreateId">String containig eventId if it already exist, else null.</param>
        /// <returns>Returns the id of the created event, should be stocked for future uses of the event (update/delete).</returns>
        public string CreateOrUpdateEventInCalendar(CalendarConnectionInformations connectionInformations, EventEntry eventToCreate, string eventToCreateId)
        {           
            if(eventToCreateId!=null)
            {
               return UpdateEvent(connectionInformations, eventToCreate, eventToCreateId);               
            }
            else
                return CreateEventInCalendar(connectionInformations, eventToCreate);
        }


        /// <summary>
        /// Create an event in multiple calendars if they don't already exist, else update them.
        /// Returns a list of the id of the created event in each calendar, should be stocked for future uses of the event (update/delete).
        /// </summary>
        /// <param name="eventToCreateIdList">If the event already exist, it's a list of string containig eventId of each calendar, else it's null.</param>
        /// <returns> Returns failed event ids.</returns>
        public List<string> UpdateEventInMultipleCalendars(List<CalendarConnectionInformations> connectionInformationsList, EventEntry eventToCreate, List<string> eventToCreateIdList)
        {
            List<string> eventIdList = new List<string>();
            int i = 0;
            foreach (var connectionInformation in connectionInformationsList)
            {

                string eventId =UpdateEvent(connectionInformation, eventToCreate, eventToCreateIdList[i]);
                if (eventId==null)
                {
                    eventIdList.Add(eventToCreateIdList[i]);
                }
                i++;
            }
            return eventIdList;
        }


        public void DeleteEvent(CalendarConnectionInformations connectionInformations, string eventToDeleteId)
        {
            CalendarService service = new CalendarService("");
            service.setUserCredentials(connectionInformations.username, connectionInformations.password);

            EventEntry eventRetrieved = new EventEntry();
            eventRetrieved = retrieveEventFromEventId(eventToDeleteId, service);

            if (eventRetrieved != null)
            {
                eventRetrieved.Delete();
            }

        }


        public void DeleteEventInMultipleCalendars(List<CalendarConnectionInformations> connectionInformationsList, List<string> eventToDeleteIdList)
        {
            int i = 0;
            foreach (var connectionInformation in connectionInformationsList)
            {
                DeleteEvent(connectionInformation, eventToDeleteIdList[i]);
                i++;
            }
        }


        /// <summary>
        /// Returns the event corresponding to the event id.
        /// </summary>
        /// <returns>Returns the event corresponding to the event id.</returns>
        private EventEntry retrieveEventFromEventId(string originalEventId, CalendarService service)
        {
            EventEntry eventRetrieved = new EventEntry();

            // Create the query object:
            EventQuery query = new EventQuery();
            string Url = originalEventId.ToString();
            query.Uri = new Uri(Url);

            // Tell the service to query:
            EventFeed calFeed = service.Query(query);


            if (calFeed.Entries.Count > 0) 
            {
                eventRetrieved = (EventEntry)calFeed.Entries[0];    
                return eventRetrieved;
            }
           
            return null;
        }


        /// <summary>
        /// Update an already created event by changind its caracteristics to thoses of a new event.
        /// Returns the id of the event.
        /// </summary>
        /// <returns>Returns the id of the event.</returns>
        private string UpdateEvent(CalendarConnectionInformations connectionInformations, EventEntry newEvent, string originalEventId)
        {
            // Authenticate
            CalendarService service = new CalendarService("");
            service.setUserCredentials(connectionInformations.username, connectionInformations.password);

            EventEntry eventRetrieved = new EventEntry();
            eventRetrieved = retrieveEventFromEventId(originalEventId, service);
            if (eventRetrieved != null)
            {
                eventRetrieved = MergeEvents(eventRetrieved, newEvent);
                eventRetrieved.Update(); 
                return eventRetrieved.EditUri.ToString();
            }
            return null;
        }


        /// <summary>
        /// Get the caracteristics of a new event to a first event.
        /// Returns the first event modified.
        /// </summary>
        /// <returns>Returns the first event modified.</returns>
        private EventEntry MergeEvents(EventEntry baseEvent, EventEntry newEvent)
        {
            baseEvent.Title = newEvent.Title;
            baseEvent.Content = newEvent.Content;

            Where baseEventWhere = new Where();
            baseEventWhere.ValueString = newEvent.Locations.ToString();
            baseEvent.Locations.Add(baseEventWhere);

            //regler Times aussi
            //When baseEventeWhen = new When();
            //baseEventWhen.StartTime = newEvent.Times.S
            //baseEventWhen.EndTime=newEvent.Times.
            //baseEvent.Times.Add(baseEventWhen);

            //regler participant

            //+reminder?
            return baseEvent;
        }


    }
}
