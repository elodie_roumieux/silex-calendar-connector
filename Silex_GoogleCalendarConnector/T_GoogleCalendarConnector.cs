﻿using Google.GData.Client;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Silex_GoogleCalendarConnector
{
    [TestFixture]
    public class T_GoogleCalendarConnector
    {
        [TestCase]
        public void TestParseNullUri()
        {
            String s = new CalendarUtils().ParseCalendarIdFromURI(null);
            Assert.IsNull(s);
        }

        [TestCase]
        public void TestParseNonURIString()
        {
            String s = new CalendarUtils().ParseCalendarIdFromURI(new AtomUri("Hello world"));
            Assert.IsNull(s);
        }

        [TestCase]
        public void TestParseURI()
        {
            AtomUri atomUri = new AtomUri("https://www.google.com/calendar/feeds/default/private/full/1ungee1tkj2mf4m4238qhq5iu4");
            String result = new CalendarUtils().ParseCalendarIdFromURI(atomUri);
            Assert.AreEqual("1ungee1tkj2mf4m4238qhq5iu4", result);
        }
    }
}
