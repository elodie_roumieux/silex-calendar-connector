﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Silex_GoogleCalendarConnector;
using Google.GData.Calendar;
using Google.GData.Extensions;


namespace APIUsageSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            CalendarConnectionInformations myInformations = new CalendarConnectionInformations();
            myInformations.username = "roumieuxe@gmail.com";
            myInformations.password = "bAb7nPQ8";
            myInformations.feedUri = "https://www.google.com/calendar/feeds/default/private/full";          

            EventEntry rdvTest2 = new EventEntry();
            rdvTest2.Title.Text = "TestOriginal:";
            rdvTest2.Content.Content = "test des commentaires et un numéro de téléphone: 0633359920";
            Where rdvTest2Location = new Where();
            rdvTest2Location.ValueString = "23 rue Edouard Jacques, 75014 Paris";
            rdvTest2.Locations.Add(rdvTest2Location);
            When rdvTest2Time = new When();
            rdvTest2Time.StartTime = DateTime.Now;
            rdvTest2Time.EndTime = rdvTest2Time.StartTime;
            rdvTest2.Times.Add(rdvTest2Time);
            //Who rdvTest2Participant = new Who();
            //rdvTest2Participant.Email = "jr.daniault@gmail.com";
            //rdvTest2Participant.Rel = "EVENT_ATTENDEE";
            //rdvTest2.Participants.Add(rdvTest2Participant);
            Reminder reminderRdvTest2 = new Reminder();
            reminderRdvTest2.Method = Reminder.ReminderMethod.none;
            rdvTest2.Reminders.Add(reminderRdvTest2);

            GoogleCalendarConnector connectionTest1 = new GoogleCalendarConnector(new CalendarConnectionInformations());

            //string defaultId;
            //string test2Id;
            //string testAleaId;
           
            string otherCalendatTitleOnMyAccount = "stage silex test partage agenda depuis internet";


            //test2Id = connectionTest1.CreateEventInCalendar(myInformations, rdvTest2);
            //defaultId = connectionTest1.CreateEventInCalendar(myInformations, eventEntryDefaultGenerator());
            //testAleaId = connectionTest1.CreateOrUpdateEventInCalendar(myInformations, eventEntryAleaGenerator(), null);

            //rdvTest2.Title.Text = "Update du test original";
            //rdvTest2.Content.Content = "Updated";
            //test2Id = connectionTest1.CreateOrUpdateEventInCalendar(myInformations, rdvTest2, test2Id);
            //rdvTest2Location.ValueString = "address updated";
            //rdvTest2.Locations.Add(rdvTest2Location);
            //test2Id = connectionTest1.CreateOrUpdateEventInCalendar(myInformations, rdvTest2, test2Id);

            //connectionTest1.DeleteEvent(myInformations, defaultId);
            //connectionTest1.DeleteEvent(myInformations, testAleaId);
            //connectionTest1.DeleteEvent(myInformations, test2Id);

            CalendarConnectionInformations myInformationsOtherCalendar = new CalendarConnectionInformations();
            myInformationsOtherCalendar.username = myInformations.username;
            myInformationsOtherCalendar.password = myInformations.password;
            myInformationsOtherCalendar.feedUri = myInformations.getFeedUriFromCalendarTitle(myInformations.username, myInformations.password, otherCalendatTitleOnMyAccount);
            //connectionTest1.CreateEventInCalendar(myInformationsOtherCalendar, eventEntryAleaGenerator());
            
            List<CalendarConnectionInformations> ListOfCalendarsOnMyAcount = new List<CalendarConnectionInformations> { myInformations, myInformationsOtherCalendar };
            List<string> eventIdList1 =new List<string>();
            List<string> eventIdList2 = new List<string>();            
            eventIdList2 = connectionTest1.CreateEventInMultipleCalendars(ListOfCalendarsOnMyAcount, CalendarElementsGenerator.eventEntryAleaGenerator());
            connectionTest1.DeleteEventInMultipleCalendars(ListOfCalendarsOnMyAcount, eventIdList2);
         }

        static void DemoCreateEvent(){
        }

        static void DemoUpdateEvent(){
        }

        static void DemoDeleteEvent(){

        }

}
