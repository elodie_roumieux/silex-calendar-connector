﻿using Google.GData.Calendar;
using Google.GData.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIUsageSamples
{
    public static class CalendarElementsGenerator
    {
              
        /// <summary>
        /// Creates a default event:
        ///     no title,
        ///     no location,
        ///     no content,
        ///     begins and ends now,
        ///     not a recurring event,
        ///     a sms reminder (if fonction allowed on the account setting)
        /// </summary>
        public static EventEntry eventEntryDefaultGenerator()
        {
            EventEntry eventDefault = new EventEntry();
            return eventDefault;
        }

        
        /// <summary>
        /// create an event with:
        ///    a title,
        ///     a content (if phone number is add in the content section, it will be directly usable on a phone)
        ///     a location (if real address, it is directly linked to google maps)
        ///     a start time: now
        ///     a end time: 30min after start time
        ///     no recurrence,
        ///     a possible participant if Email field is competed with a gmail address (note: it won't work if Rel field isn't defined)
        ///         a participant can see the event in its main callendar but can't modify it (could be used for clients?)
        ///      a reminder played 15 minutes before the event through an Agenda alert (can also be an email , a sms -default- or all)
        /// </summary>
        public static EventEntry eventEntryGenerator()
        {
            EventEntry rdvTest1 = new EventEntry();
            rdvTest1.Title.Text = "Premier test de création d'un rendez-vous";
            rdvTest1.Content.Content = "Il s'agit d'un rendez-vous factice";
            Where rdvTest1Location = new Where();
            rdvTest1Location.ValueString = "adresse";
            rdvTest1.Locations.Add(rdvTest1Location);
            When rdvTest1Time = new When();
            rdvTest1Time.StartTime = DateTime.Now;
            rdvTest1Time.EndTime = rdvTest1Time.StartTime.AddMinutes(30);
            rdvTest1.Times.Add(rdvTest1Time);
            //Who rdvTest1Participant = new Who();
            //rdvTest1Participant.Email = "     @gmail.com";
            //rdvTest1Participant.Rel = "EVENT_PERFORMER";
            //rdvTest1.Participants.Add(rdvTest1Participant);
            Reminder reminderRdvTest1 = new Reminder();
            reminderRdvTest1.Days = 1;
            reminderRdvTest1.Minutes = 15;
            reminderRdvTest1.Method = Reminder.ReminderMethod.alert;
            rdvTest1.Reminders.Add(reminderRdvTest1);

            return rdvTest1;
        }

        
        /// <summary>
        /// create a random event:
        ///      random title, content and location,
        ///      random begining between now and 4 days later
        ///      random end up to on day after the begining
        ///      no reminder
        /// </summary>
        public static EventEntry eventEntryAleaGenerator()
        {
            EventEntry eventAlea = new EventEntry();
            eventAlea.Title.Text = randomString(8);
            eventAlea.Content.Content = randomString(10);
            Where eventAleaLocation = new Where();
            eventAleaLocation.ValueString = randomString(6);
            eventAlea.Locations.Add(eventAleaLocation);
            When eventAleaTime = new When();
            eventAleaTime.StartTime = DateTime.Now.AddDays(random.Next(0,4));
            eventAleaTime.StartTime = eventAleaTime.StartTime.AddHours(random.Next(0,6));
            eventAleaTime.StartTime = eventAleaTime.StartTime.AddMinutes(random.Next(0,59));
            eventAleaTime.EndTime = eventAleaTime.StartTime.AddMinutes(random.Next(0,59));
            eventAleaTime.EndTime = eventAleaTime.EndTime.AddHours(random.Next(0,15));
            eventAleaTime.EndTime = eventAleaTime.EndTime.AddSeconds(random.Next(0,59));
            eventAleaTime.EndTime = eventAleaTime.EndTime.AddDays(random.Next(0,1));
            eventAlea.Times.Add(eventAleaTime);
            Reminder eventAleaReminder = new Reminder();
            eventAleaReminder.Method = Reminder.ReminderMethod.none;
            eventAlea.Reminders.Add(eventAleaReminder);


            return eventAlea;
        }


        public static Random random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Creates a random string of fixed length.
        /// </summary>
        public static string randomString(int length)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for(int i=0;i<length;i++){
                ch = (char)random.Next('A', 'Z');
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }
    }
}
